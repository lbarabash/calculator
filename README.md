# Abstract

Simple calculator that performs plus, minus, multiply, and divide operations with integer and floating point numbers.

# Install

git clone git@bitbucket.org:lbarabash/calculator.git

cd calculator

npm install

npm start

Open http://localhost:3000/ in your browser.

# Details

Based on the https://github.com/davezuko/react-redux-starter-kit boilerplate repository.

Touched files in directories:

src/layouts/PageLayout

src/components

Everything else is not touched :)
